#!/usr/bin/env bash

set -xe

VERSION=${1:-master}

for arch in amd64 arm64 mips64le arm 386; do
    if [ ! -e .done_$arch ]; then
	TAG=registry.gitlab.com/trenta3/docker-xmake/linux/$arch:master
	if [[ "$arch" == "386" ]]; then
	    BUILD_ARCH="i386"
	    buildah bud --arch $arch --build-arg VERSION=$VERSION --build-arg BUILD_ARCH=$BUILD_ARCH -t $TAG  .
	else
	    buildah bud --arch $arch --build-arg VERSION=$VERSION -t $TAG  .

	fi
	buildah push $TAG
	touch .done_$arch
    else
	echo "SKIPPING ALREADY BUILT $arch"
    fi
done
