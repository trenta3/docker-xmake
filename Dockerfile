FROM debian:stable

ARG VERSION=master
ARG ARCH

ENV XMAKE_ROOT=y

SHELL ["/bin/sh", "-c"]
RUN rm -rf /var/cache/* && \
    apt update -y && apt install -y git wget bash build-essential \
    libncurses6 libtinfo6 libreadline8 libc6-dev
RUN git clone --recursive https://github.com/xmake-io/xmake -b $VERSION && \
    cd xmake && make build BUILD_ARCH=$ARCH && \
    make install BUILD_ARCH=$ARCH && \
    cd .. && rm -rf xmake
