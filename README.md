# Docker images for [xmake](https://xmake.io)
I needed Docker images for many architectures with xmake already installed, so I did it.
You can see the images in Gitlab Container Registry, i.e.
```
registry.gitlab.com/trenta3/docker-xmake:${BRANCH_OR_TAG_NAME}
```
